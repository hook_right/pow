FROM golang:1.22 as builder

WORKDIR /app

COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

ENV CGO_ENABLED=0

COPY . .

RUN go build -o server ./cmd/server

FROM scratch

WORKDIR /app

COPY --from=builder /app/server .

CMD ["./server"]
