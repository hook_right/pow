# POW

A simple service that implements ddos protection through proof of work. After authorization, the service gives a random quote.

## Algorithm
The service uses the popular hashcach algorithm.
The main advantages of this algorithm:
- Does not require real money from senders and recipients.
- Ease of implementation for clients.

## Server config
The service is configured using a yaml file. The file should be in the current directory and called pow.server.yaml
```yaml
server:
  address: :9965 # listen address
  solutionTimeout: 2m # the duration during which the service waits for a decision from the client

pow:
  prefixSize: 4 # prefix size in bytes that should be used to generate the hash
  nonceSize: 16 # size in bytes of the solution that the client should issue, given that the initial 8 bytes are the timestamp
  difficult: 20 # number of initial hash bits that must be zero
  nonceValidity: 1m # allowed time gap used in nonce

logLevel: DEBUG # if set DEBUG will use the log level DEBUG otherwise it will use the INFO level
```

## Running
### Binary

- build client and serve `make build`
- put the server config file into current directory
- start server `./server`
- start client `./client <server address>`

### Docker
- build docker images `make build docker`
- start server container `docker run -v /path/to/config:/app/pow.server.yaml pow-server`
- start client container `docker run pow-client <server address>`
