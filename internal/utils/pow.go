package utils

import (
	"encoding/binary"
	"gitlab.com/hook_right/pow/pkg/pow"
)

func MarshalProblem(p pow.Problem) []byte {
	b := make([]byte, 1+len(p.Prefix)+2+2) // prefix size + prefix + difficult + nonce size
	b[0] = byte(len(p.Prefix))
	copy(b[1:], p.Prefix)
	binary.LittleEndian.PutUint16(b[1+len(p.Prefix):], uint16(p.Difficult))
	binary.LittleEndian.PutUint16(b[1+len(p.Prefix)+2:], uint16(p.NonceSize))
	return b
}
