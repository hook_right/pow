package repository

import "math/rand/v2"

type QuotesRepository interface {
	GetRandom() string
}

type staticQuotesRepository struct{}

func NewStaticQuotesRepository() QuotesRepository {
	return &staticQuotesRepository{}
}

func (r *staticQuotesRepository) GetRandom() string {
	return quoutes[rand.Int()%len(quoutes)]
}
