package repository

var quoutes = []string{
	`"If life were predictable, it would cease to be life and be without flavor." — Eleanor Roosevelt`,
	`"Life is 10% what happens to me and 90% of how I react to it."— Charles Swindoll`,
	`"In the end, it's not the years in your life that count. It's the life in your years." — Abraham Lincoln`,
	`"Life is a succession of lessons which must be lived to be understood." — Ralph Waldo Emerson`,
	`"You will face many defeats in life, but never let yourself be defeated." — Maya Angelou`,
	`"If you look at what you have in life, you'll always have more. If you look at what you don't have in life, you'll never have enough." — Oprah Winfrey`,
	`"Never let the fear of striking out keep you from playing the game." — Babe Ruth`,
	`"Life is never fair, and perhaps it is a good thing for most of us that it is not." — Oscar Wilde`,
	`"The only impossible journey is the one you never begin." — Tony Robbins`,
	`"In this life we cannot do great things. We can only do small things with great love." — Mother Teresa`,
	`"Only a life lived for others is a life worthwhile." — Albert Einstein`,
	`"Life is what happens when you're busy making other plans." — John Lennon`,
	`"You only live once, but if you do it right, once is enough." — Mae West`,
	`"Live in the sunshine, swim the sea, drink the wild air." — Ralph Waldo Emerson`,
	`"Go confidently in the direction of your dreams! Live the life you've imagined." — Henry David Thoreau`,
	`"The greatest glory in living lies not in never falling, but in rising every time we fall." — Nelson Mandela`,
	`"Life is really simple, but we insist on making it complicated." — Confucius`,
	`"Life itself is the most wonderful fairy tale." — Hans Christian Andersen`,
	`"Do not let making a living prevent you from making a life." — John Wooden`,
	`"Life is ours to be spent, not to be saved." — D. H. Lawrence`,
	`"Keep smiling, because life is a beautiful thing and there's so much to smile about." — Marilyn Monroe`,
	`"Life is a long lesson in humility." — James M. Barrie`,
	`"In three words I can sum up everything I've learned about life: It goes on." — Robert Frost`,
	`"Love the life you live. Live the life you love." — Bob Marley`,
	`"Life is either a daring adventure or nothing at all." — Helen Keller`,
	`"You have brains in your head. You have feet in your shoes. You can steer yourself any direction you choose." — Dr. Seuss`,
	`"Life is made of ever so many partings welded together." — Charles Dickens`,
	`"Life is trying things to see if they work." — Ray Bradbury`,
	`"Life isn't about finding yourself. Life is about creating yourself." — George Bernard Shaw`,
	`"Many of life's failures are people who did not realize how close they were to success when they gave up." — Thomas Edison`,
}
