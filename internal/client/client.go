package client

import (
	"context"
	"encoding/binary"
	"fmt"
	"gitlab.com/hook_right/pow/pkg/pow"
	"io"
	"net"
)

type Client struct {
	addr   string
	solver pow.Solver
}

func New(addr string) *Client {
	return &Client{
		addr:   addr,
		solver: pow.NewSolver(),
	}
}

func (c *Client) GetQuote(ctx context.Context) (string, error) {
	conn, err := net.Dial("tcp", c.addr)
	if err != nil {
		return "", fmt.Errorf("failed to dial %s: %w", c.addr, err)
	}
	defer conn.Close()

	buf := make([]byte, 128)

	p, err := readProblem(conn, buf)
	if err != nil {
		return "", err
	}

	nonce, err := c.solver.Solve(ctx, p)
	if err != nil {
		return "", fmt.Errorf("failed to solve problem: %w", err)
	}

	if _, err := conn.Write(nonce); err != nil {
		return "", fmt.Errorf("failed to write nonce to server: %w", err)
	}

	quote, err := readQuote(conn, buf)
	if err != nil {
		return "", err
	}

	return string(quote), nil
}
func readProblem(conn net.Conn, buf []byte) (pow.Problem, error) {
	if _, err := conn.Read(buf); err != nil {
		return pow.Problem{}, fmt.Errorf("failed to read problem: %w", err)
	}

	return pow.Problem{
		Prefix:    buf[1 : 1+int(buf[0])],
		Difficult: int(binary.LittleEndian.Uint16(buf[1+int(buf[0]):])),
		NonceSize: int(binary.LittleEndian.Uint16(buf[1+int(buf[0])+2:])),
	}, nil
}

func readQuote(conn net.Conn, buf []byte) ([]byte, error) {
	if _, err := conn.Read(buf[:2]); err != nil {
		return nil, fmt.Errorf("failed to read quote length: %w", err)
	}

	quoteLen := int(binary.LittleEndian.Uint16(buf))

	if len(buf) < quoteLen {
		buf = make([]byte, quoteLen)
	}

	if _, err := io.ReadAtLeast(conn, buf, quoteLen); err != nil {
		return nil, fmt.Errorf("failed to read quote: %w", err)
	}

	return buf[:quoteLen], nil
}
