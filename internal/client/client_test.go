package client

import (
	"context"
	"encoding/binary"
	"github.com/stretchr/testify/require"
	"gitlab.com/hook_right/pow/internal/utils"
	"gitlab.com/hook_right/pow/pkg/pow"
	"net"
	"testing"
)

func TestClient(t *testing.T) {
	ln, err := net.Listen("tcp", ":0")
	require.NoError(t, err)
	defer ln.Close()

	expectedProblem := pow.Problem{
		Prefix:    []byte{1, 2, 3, 4},
		Difficult: 20,
		NonceSize: 16,
	}

	solution := []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193}

	expectedQuote := "test quote"

	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				require.ErrorIs(t, err, net.ErrClosed)
				return
			}

			_, err = conn.Write(utils.MarshalProblem(expectedProblem))
			require.NoError(t, err)

			buf := make([]byte, 16)
			_, err = conn.Read(buf)
			require.NoError(t, err)

			require.Equal(t, solution, buf)

			buf = make([]byte, len(expectedQuote)+2)
			binary.LittleEndian.PutUint16(buf, uint16(len(expectedQuote)))
			copy(buf[2:], expectedQuote)

			_, err = conn.Write(buf)
			require.NoError(t, err)
		}
	}()

	c := New(ln.Addr().String())
	c.solver = &testSolver{
		expectedProblem: expectedProblem,
		solution:        solution,
		t:               t,
	}

	quote, err := c.GetQuote(context.Background())
	require.NoError(t, err)
	require.Equal(t, expectedQuote, quote)
}

type testSolver struct {
	expectedProblem pow.Problem
	solution        []byte
	t               *testing.T
}

func (s *testSolver) Solve(ctx context.Context, p pow.Problem) ([]byte, error) {
	s.t.Helper()
	require.Equal(s.t, s.expectedProblem, p)
	return s.solution, nil
}
