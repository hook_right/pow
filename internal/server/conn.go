package server

import (
	"encoding/binary"
	"gitlab.com/hook_right/pow/internal/utils"
	"go.uber.org/zap"
	"net"
	"time"
)

type conn struct {
	srv *Server
	rwc net.Conn
}

func (s *Server) newConn(rwc net.Conn) *conn {
	return &conn{
		rwc: rwc,
		srv: s,
	}
}

func (c *conn) serve() {
	c.srv.logger.Info("got new connection", zap.String("remote_addr", c.rwc.RemoteAddr().String()))
	defer c.rwc.Close()

	p, err := c.srv.problemGenerator.CreateProblem()
	if err != nil {
		c.srv.logger.Error("failed to create problem", zap.String("remote_addr", c.rwc.RemoteAddr().String()), zap.Error(err))
		return
	}

	if _, err := c.rwc.Write(utils.MarshalProblem(p)); err != nil {
		c.srv.logger.Info("failed to write problem", zap.String("remote_addr", c.rwc.RemoteAddr().String()), zap.Error(err))
		return
	}

	_ = c.rwc.SetReadDeadline(time.Now().Add(c.srv.solutionTimeout))

	solution := make([]byte, p.NonceSize)
	if _, err := c.rwc.Read(solution); err != nil {
		c.srv.logger.Info("failed to read solution", zap.String("remote_addr", c.rwc.RemoteAddr().String()), zap.Error(err))
		return
	}

	if err := c.srv.problemGenerator.Verify(p, solution); err != nil {
		c.srv.logger.Info("failed to verify solution", zap.Error(err), zap.String("remote_addr", c.rwc.RemoteAddr().String()))
		return
	}

	quote := c.srv.quotesRepo.GetRandom()

	res := make([]byte, 2+len(quote))
	binary.LittleEndian.PutUint16(res, uint16(len(quote)))
	copy(res[2:], quote)

	_, _ = c.rwc.Write(res)
	c.srv.logger.Info("connection close normally", zap.String("remote_addr", c.rwc.RemoteAddr().String()))
}
