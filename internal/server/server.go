package server

import (
	"errors"
	"fmt"
	"gitlab.com/hook_right/pow/internal/repository"
	"gitlab.com/hook_right/pow/pkg/pow"
	"go.uber.org/zap"
	"net"
	"sync"
	"time"
)

type Server struct {
	addr             string
	solutionTimeout  time.Duration
	problemGenerator pow.ProblemGenerator
	quotesRepo       repository.QuotesRepository
	logger           *zap.Logger

	ln net.Listener
	wg sync.WaitGroup
}

func New(
	addr string,
	solutionTimeout time.Duration,
	problemGenerator pow.ProblemGenerator,
	quotesRepo repository.QuotesRepository,
	logger *zap.Logger,
) *Server {
	return &Server{
		addr:             addr,
		solutionTimeout:  solutionTimeout,
		problemGenerator: problemGenerator,
		quotesRepo:       quotesRepo,
		logger:           logger,
	}
}

func (s *Server) Serve() error {
	ln, err := net.Listen("tcp", s.addr)
	if err != nil {
		return fmt.Errorf("failed to listen: %w", err)
	}
	s.ln = ln

	s.logger.Info("server started")
	s.wg.Add(1)
	defer func() {
		s.wg.Done()
		s.logger.Info("server stopped")
	}()
	for {
		rw, err := ln.Accept()
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				return nil
			}
			return fmt.Errorf("failed to accept: %w", err)
		}

		c := s.newConn(rw)
		go c.serve()
	}
}

func (s *Server) Shutdown() {
	s.ln.Close()
	s.wg.Wait()
}
