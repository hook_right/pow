package server

import (
	"encoding/binary"
	"github.com/stretchr/testify/require"
	"gitlab.com/hook_right/pow/pkg/pow"
	"go.uber.org/zap"
	"io"
	"net"
	"slices"
	"testing"
	"time"
)

func TestServer(t *testing.T) {
	testCases := []struct {
		name          string
		problem       pow.Problem
		expectedNonce []byte
		nonce         []byte
		wantQuote     bool
	}{
		{
			name: "valid",
			problem: pow.Problem{
				Prefix:    []byte{2, 3, 4, 5},
				Difficult: 20,
				NonceSize: 16,
			},
			expectedNonce: []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193},
			nonce:         []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193},
			wantQuote:     true,
		},
		{
			name: "invalid",
			problem: pow.Problem{
				Prefix:    []byte{2, 3, 4, 5},
				Difficult: 20,
				NonceSize: 16,
			},
			expectedNonce: []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193},
			nonce:         []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 194},
			wantQuote:     false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			const testQuote = "test quote"

			srv := New(
				":0",
				100*time.Millisecond,
				&testProblemGenerator{
					problem:       tc.problem,
					expectedNonce: tc.expectedNonce,
				},
				&testQuotesRepo{quote: testQuote},
				zap.NewNop(),
			)
			go srv.Serve()
			defer srv.Shutdown()
			time.Sleep(10 * time.Millisecond)

			c, err := net.Dial("tcp", srv.ln.Addr().String())
			require.NoError(t, err)

			c.SetReadDeadline(time.Now().Add(20 * time.Millisecond))

			buf := make([]byte, 128)

			_, err = c.Read(buf)
			require.NoError(t, err)

			p := pow.Problem{
				Prefix:    buf[1 : 1+int(buf[0])],
				Difficult: int(binary.LittleEndian.Uint16(buf[1+int(buf[0]):])),
				NonceSize: int(binary.LittleEndian.Uint16(buf[1+int(buf[0])+2:])),
			}

			require.Equal(t, tc.problem, p)

			_, err = c.Write(tc.nonce)
			require.NoError(t, err)

			if tc.wantQuote {
				c.SetReadDeadline(time.Now().Add(20 * time.Millisecond))

				_, err = c.Read(buf)
				require.NoError(t, err)

				require.Equal(t, testQuote, string(buf[2:len(testQuote)+2]))
			} else {
				_, err = c.Read(buf)
				require.Equal(t, io.EOF, err)
			}
		})
	}
}

type testQuotesRepo struct {
	quote string
}

func (r *testQuotesRepo) GetRandom() string {
	return r.quote
}

func (r *testQuotesRepo) Set(quote string) {
	r.quote = quote
}

type testProblemGenerator struct {
	problem       pow.Problem
	expectedNonce []byte
}

func (g *testProblemGenerator) CreateProblem() (pow.Problem, error) {
	return g.problem, nil
}

func (g *testProblemGenerator) Verify(p pow.Problem, nonce []byte) error {
	if slices.Equal(g.expectedNonce, nonce) {
		return nil
	}
	return pow.ErrNonceInvalid
}
