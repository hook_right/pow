package main

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/hook_right/pow/internal/repository"
	"gitlab.com/hook_right/pow/internal/server"
	"gitlab.com/hook_right/pow/pkg/pow"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	viper.SetConfigName("pow.server")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("failed to read config: %s\n", err)
		os.Exit(1)
	}

	viper.SetDefault("pow.prefixSize", 4)
	viper.SetDefault("pow.nonceSize", 16)
	viper.SetDefault("pow.difficult", 20)
	viper.SetDefault("pow.nonceValidity", 1*time.Minute)

	problemGenerator := pow.NewProblemGenerator(
		viper.GetInt("pow.prefixSize"),
		viper.GetInt("pow.nonceSize"),
		viper.GetInt("pow.difficult"),
		viper.GetDuration("pow.nonceValidity"),
	)

	quotesRepo := repository.NewStaticQuotesRepository()

	var logger *zap.Logger
	if viper.GetString("logLevel") == "DEBUG" {
		logger, _ = zap.NewDevelopment()
	} else {
		logger, _ = zap.NewProduction()
	}

	viper.SetDefault("server.solutionTimeout", 2*time.Minute)
	srv := server.New(
		viper.GetString("server.address"),
		viper.GetDuration("server.solutionTimeout"),
		problemGenerator,
		quotesRepo,
		logger,
	)

	go func() {
		if err := srv.Serve(); err != nil {
			logger.Fatal("failed to serve", zap.Error(err))
		}
	}()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-interrupt
	logger.Info("received system signal, server will be shutdown")

	srv.Shutdown()
}
