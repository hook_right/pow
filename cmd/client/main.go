package main

import (
	"context"
	"fmt"
	"gitlab.com/hook_right/pow/internal/client"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		usage()
		os.Exit(1)
	}

	addr := os.Args[1]

	c := client.New(addr)

	quote, err := c.GetQuote(context.Background())
	if err != nil {
		fmt.Println("failed to get quote, reason: ", err)
		os.Exit(2)
	}

	fmt.Println(quote)
}

func usage() {
	fmt.Println("Usage: client <address to the server>")
}
