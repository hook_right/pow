build: build-client build-server

test:
	go test ./...

test-verbose:
	go test -v ./...

build-client:
	go build -o client ./cmd/client
build-server:
	go build -o server ./cmd/server

build-client-docker:
	docker build -t pow-client -f client.Dockerfile .
build-server-docker:
	docker build -t pow-server -f server.Dockerfile .

build-docker: build-client-docker build-server-docker