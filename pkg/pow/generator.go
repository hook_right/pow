package pow

import (
	"crypto/rand"
	"encoding/binary"
	"io"
	"time"
)

type ProblemGenerator interface {
	CreateProblem() (Problem, error)
	Verify(Problem, []byte) error
}

type problemGenerator struct {
	prefixSize    int
	difficult     int
	nonceSize     int
	nonceValidity time.Duration

	nower      inower
	randReader io.Reader
}

func NewProblemGenerator(prefixSize int, nonceSize int, difficult int, nonceValidity time.Duration) ProblemGenerator {
	return &problemGenerator{
		prefixSize:    prefixSize,
		nonceSize:     nonceSize,
		nonceValidity: nonceValidity,
		difficult:     difficult,
		nower:         nowerTime{},
		randReader:    rand.Reader,
	}
}

func (g *problemGenerator) CreateProblem() (Problem, error) {
	prefix := make([]byte, g.prefixSize)
	_, err := g.randReader.Read(prefix)
	if err != nil {
		return Problem{}, err
	}
	return Problem{
		Prefix:    prefix,
		Difficult: g.difficult,
		NonceSize: g.nonceSize,
	}, nil
}

func (g *problemGenerator) Verify(p Problem, nonce []byte) error {
	if len(nonce) != p.NonceSize {
		return ErrNonceSizeIncorrect
	}

	timestamp := binary.LittleEndian.Uint64(nonce)
	t := time.Unix(int64(timestamp), 0)

	if g.nower.Now().UTC().Sub(t) > g.nonceValidity {
		return ErrNonceExpired
	}
	return verify(p, nonce)
}
