package pow

import (
	"github.com/stretchr/testify/require"
	"io"
	"testing"
	"time"
)

func TestProblemGenerator_CreateProblem(t *testing.T) {
	nower := testNower{}
	rand := testRandReader{}
	g := createGenerator(&nower, &rand)

	rand.Set([]byte{2, 3, 4, 5})

	p, err := g.CreateProblem()
	require.NoError(t, err)

	require.Equal(t, Problem{
		Prefix:    []byte{2, 3, 4, 5},
		Difficult: 20,
		NonceSize: 16,
	}, p)
}

func TestProblemGenerator_Verify(t *testing.T) {
	testCases := []struct {
		name    string
		problem Problem
		nonce   []byte
		now     time.Time
		want    error
	}{
		{
			name: "valid",
			problem: Problem{
				Prefix:    []byte{98, 75, 67, 73},
				Difficult: 20,
				NonceSize: 16,
			},
			now:   time.Unix(1713723600, 0),
			nonce: []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193},
			want:  nil,
		},
		{
			name: "expired",
			problem: Problem{
				Prefix:    []byte{98, 75, 67, 73},
				Difficult: 20,
				NonceSize: 16,
			},
			now:   time.Now(),
			nonce: []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193},
			want:  ErrNonceExpired,
		},
		{
			name: "invalid size",
			problem: Problem{
				Prefix:    []byte{98, 75, 67, 73},
				Difficult: 20,
				NonceSize: 16,
			},
			nonce: []byte{221, 88, 37, 102, 0, 0, 0, 0, 68, 89, 249, 0, 116, 149, 90, 193, 200},
			want:  ErrNonceSizeIncorrect,
		},
		{
			name: "not enough zeros",
			problem: Problem{
				Prefix:    []byte{199, 53, 111, 145},
				Difficult: 20,
				NonceSize: 16,
			},
			nonce: []byte{171, 120, 37, 102, 164, 61, 83, 123, 152, 170, 15, 96, 159, 40, 53, 147},
			want:  ErrNonceInvalid,
		},
		{
			name: "not enough zeros in last byte",
			problem: Problem{
				Prefix:    []byte{153, 229, 188, 252},
				Difficult: 20,
				NonceSize: 16,
			},
			nonce: []byte{187, 119, 37, 102, 204, 144, 39, 12, 177, 182, 87, 205, 141, 252, 235, 39},
			want:  ErrNonceInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			n := testNower{}
			n.Set(tc.now)
			g := createGenerator(&n, &testRandReader{})
			require.Equal(t, tc.want, g.Verify(tc.problem, tc.nonce))
		})
	}
}

func createGenerator(n inower, r io.Reader) ProblemGenerator {
	p := problemGenerator{
		prefixSize:    4,
		difficult:     20,
		nonceSize:     16,
		nonceValidity: 1 * time.Minute,
		nower:         n,
		randReader:    r,
	}
	return &p
}

type testNower struct {
	now time.Time
}

func (t *testNower) Now() time.Time {
	return t.now
}

func (t *testNower) Set(now time.Time) {
	t.now = now
}

type testRandReader struct {
	rand []byte
}

func (t *testRandReader) Read(p []byte) (int, error) {
	copy(p, t.rand)
	return len(t.rand), nil
}

func (t *testRandReader) Set(p []byte) {
	t.rand = p
}
