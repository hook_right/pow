package pow

import (
	"time"
)

type Problem struct {
	Prefix    []byte
	Difficult int
	NonceSize int
}

const (
	nonceValidity = 1 * time.Minute
	PrefixSize    = 4
	NonceSize     = 16
	ProblemSize   = 4 + PrefixSize
)
