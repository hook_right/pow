package pow

import (
	"context"
	"crypto/rand"
	"encoding/binary"
	"io"
)

type Solver interface {
	Solve(ctx context.Context, p Problem) ([]byte, error)
}

type solver struct {
	nower      inower
	randReader io.Reader
}

func NewSolver() Solver {
	return &solver{
		nower:      nowerTime{},
		randReader: rand.Reader,
	}
}

func (s *solver) Solve(ctx context.Context, p Problem) ([]byte, error) {
	buf := make([]byte, p.NonceSize)
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}

		if err := s.generateNonce(buf); err != nil {
			return nil, err
		}
		if verify(p, buf) == nil {
			return buf, nil
		}
	}
}

func (s *solver) generateNonce(buf []byte) error {
	binary.LittleEndian.PutUint64(buf, uint64(s.nower.Now().UTC().Unix()))
	_, err := s.randReader.Read(buf[8:])
	return err
}
