package pow

import (
	"crypto/sha256"
	"errors"
	"time"
)

var (
	ErrNonceSizeIncorrect = errors.New("nonce size is incorrect")
	ErrNonceExpired       = errors.New("nonce has expired")
	ErrNonceInvalid       = errors.New("nonce is not valid")
)

func verify(
	p Problem,
	nonce []byte,
) error {
	h := sha256.New()
	h.Write(p.Prefix)
	h.Write(nonce)
	sum := h.Sum(nil)

	off := 0
	i := 0
	for i = 0; i <= p.Difficult-8; i += 8 {
		if sum[off] != 0 {
			return ErrNonceInvalid
		}
		off++
	}
	mask := byte(0xff << (8 + i - p.Difficult))
	if (sum[off] & mask) != 0 {
		return ErrNonceInvalid
	}
	return nil
}

type inower interface {
	Now() time.Time
}

type nowerTime struct{}

func (n nowerTime) Now() time.Time { return time.Now() }
