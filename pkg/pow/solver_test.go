package pow

import (
	"context"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestSolver_Solve(t *testing.T) {
	testCases := []struct {
		name     string
		deadline time.Duration
		problem  Problem
		wantErr  error
	}{
		{
			name: "valid",
			problem: Problem{
				Prefix:    []byte{98, 75, 67, 73},
				Difficult: 8,
				NonceSize: 16,
			},
			wantErr: nil,
		},
		{
			name:     "deadline",
			deadline: 1 * time.Millisecond,
			problem: Problem{
				Prefix:    []byte{98, 75, 67, 73},
				Difficult: 30,
				NonceSize: 16,
			},
			wantErr: context.DeadlineExceeded,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s := NewSolver()
			ctx := context.Background()
			if tc.deadline > 0 {
				var cancel context.CancelFunc
				ctx, cancel = context.WithTimeout(ctx, tc.deadline)
				defer cancel()
			}

			res, err := s.Solve(ctx, tc.problem)
			require.Equal(t, tc.wantErr, err)
			if tc.wantErr == nil {
				require.Len(t, res, tc.problem.NonceSize)
			}
		})
	}
}
